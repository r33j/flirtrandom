import axios from 'axios';

const API_URL = 'http://localhost:'+process.env.VUE_APP_API_PORT+'/users/';

class AuthService {
    login(user) {
        return axios
            .post(API_URL + 'login', {
                email: user.email,
                password: user.password
            })
            .then(response => {
                if (response.data.accessToken) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                    console.log(localStorage.setItem('user', JSON.stringify(response.data)));
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem('user');
    }

    register(user) {
        return axios.post(API_URL + 'signup', {
            email: user.email,
            password: user.password
        });
    }
}

export default new AuthService();