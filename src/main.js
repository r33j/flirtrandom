import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import { store } from './store';
import VeeValidate from 'vee-validate';
import routes from './routes';
import Vuesax from 'vuesax'
import VueChatScroll from 'vue-chat-scroll'
import VueContentPlaceholders from 'vue-content-placeholders'
import vuescroll from 'vuescroll';
import 'vuesax/dist/vuesax.css' //Vuesax styles

// You can set global config here.
Vue.use(vuescroll, {
  ops: {
    // The global config
  },
  name: 'myScroll' // customize component name, default -> vueScroll
});
Vue.use(VueContentPlaceholders)

Vue.use(Vuesax, {
  // options here
})
Vue.use(VeeValidate);
Vue.use(VueChatScroll)

Vue.use(VueRouter)
Vue.use(Vuex);
import 'vue-cam'


Vue.config.productionTip = false


const router = new VueRouter({mode: 'history', routes,
  base: process.env.BASE_URL,
});


router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !store.state.auth.status.loggedIn) next('/')
  else next();
});

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
