import Vue from 'vue';
import Vuex from 'vuex';
import { auth } from './service/auth.module';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        auth
    },
    state: {
        navbarBtn: 1,
        userInfo: {}
    },
    mutations: {},
    actions: {},
    getters: {},
});