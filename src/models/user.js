export default class User {
    constructor(email, password, premium) {
        this.email = email;
        this.password = password;
        this.premium = premium;
    }
}