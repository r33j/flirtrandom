import Landing from "./components/Landing";


const routes = [
    { path: '/', component: Landing, name: 'landing'},
    { path: '/register', component: () => import("./components/Register"), name: 'register'},
    { path: '/dashboard', component: () => import("./components/Dashboard"), name: 'dashboard',
        meta: {
            requiresAuth: true
            }
        },
    { path: '/login', component: () => import("./components/Login"), name: 'login'},

    {   path: '/profile',
        component: () => import("./components/Profile"),
        name: 'profile',
        meta: {
            requiresAuth: true
            }
        },


]


export default routes;